### Monitoring

The Flask App is deployed in Kubernetes and monitored by Prometheus and Grafana.

The Grafana Dashboard includes the following metrics:

- Latency
- Traffic
- Error Rate
- CPU Usage
- Memory Usage

![Grafana Dashboard](images/1.png)

There are also alerts set up if above metrics exceed certain thresholds.

![Grafana Alerts](images/2.png)

The alert is delivered to Telegram.

![Telegram Alert](images/3.png)

### Logging

The logs are collected by Promtail and stored in Loki. The logs are queried using Grafana.

![Loki Logs](images/4.png)

### Diagram

Below is the diagram of the monitoring and logging setup.

![Monitoring and Logging](images/5.png)

Below is the stack running in the Kubernetes cluster.

![Kubernetes Stack](images/6.png)
