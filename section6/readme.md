### CICD with Gitlab

There are repos for this section:

- frontend: https://gitlab.com/jphuc96/socone-section6-frontend
- backend: https://gitlab.com/jphuc96/socone-section6-backend
- gitops: https://gitlab.com/jphuc96/socone-section6-gitops

ArgoCD is used for GitOps. the gitops repo contains the k8s manifests for the frontend and backend services and is synced to the cluster.

#### ArgoCD Dashboard

![section6](./images/3.png)

![section6](./images/4.png)

#### Monitoring

![section6](./images/1.png)

#### Logging

![section6](./images/2.png)

**Note:** integration test and e2e test have not been implemented due to the lack of resources.
