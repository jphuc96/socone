### Basic Task

In this basic task, Traefik ingress controller is deployed in the cluster, and cert-manager is used as certificate issuer
The resource files is located in `kubernetes/basic` folder.

The Flask application returns a simple message "Hello, World! V1" in the browser

![basic](images/basic.png)

### Advanced Task

In advanced task, Istio is deployed as service mesh platform, there are two versions of the application, and traffic is split between the two versions. There are several resources created in the cluster, including VirtualService, Gateway.

There are Istio addons deployed in the cluster, including Kiali, Jaeger, Grafana and Prometheus.

The resource files is located in `kubernetes/advanced` folder.

Cert-manager is used to issue certificates for the application, and the certificates are used in the Gateway resource.

In this advanced task, the traffic is split between two versions of the application with 50% each, showing the message "Hello, World! V1" and "Hello, World! V2" in the browser.

![advanced](images/advanced7.png)

![advanced](images/advanced1.png)

![advanced](images/advanced2.png)

We can monitor the traffic split in the Kiali dashboard

![kiali](images/advanced3.png)

From Kiali dashboard, we can collect log, metrics, and trace data for services

![kiali](images/advanced4.png)

![kiali](images/advanced5.png)

![kiali](images/advanced6.png)
