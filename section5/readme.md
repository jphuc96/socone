## Config firewall rules

```
# Allow all VMs to access the internet
iptables -t nat -A POSTROUTING -s 192.168.1.0/24 -o eth0 -j MASQUERADE

# Permit SSH from QEMU VM1 to QEMU VM3
iptables -A FORWARD -s 192.168.1.11 -d 192.168.1.21 -p tcp --dport 22 -j ACCEPT

# Block access from QEMU VM1 to QEMU VM2
iptables -A FORWARD -s 192.168.1.11 -d 192.168.1.12 -j DROP
```
