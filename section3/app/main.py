from flask import Flask, request

app = Flask(__name__)

@app.route('/', methods=['GET'])
def hello_world():
  return 'Hello, World! V2'

if __name__ == '__main__':
  app.run()
  app.run(host='0.0.0.0', port=5000)