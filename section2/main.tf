provider "aws" {
  profile = "default"
  region  = "ap-southeast-1"
}

terraform {
  backend "s3" {
    bucket = "hgphuc.terraform-state"
    key    = "terraform.tfstate"
    region = "ap-southeast-1"
  }
}

locals {
  tags = {
    terraform   = "true"
    Environment = var.env
  }
}

### VPC
module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.6.0"

  name = "${var.env}-${var.vpc_name}"
  cidr = var.vpc_cidr

  azs = var.vpc_azs

  public_subnets      = var.vpc_public_subnets
  private_subnets     = var.vpc_private_subnets
  enable_nat_gateway  = false
  single_nat_gateway  = false
  reuse_nat_ips       = true
  external_nat_ip_ids = aws_eip.nat[*].id

  enable_dns_hostnames = true
  enable_dhcp_options  = true

  map_public_ip_on_launch = false

  default_security_group_egress = [{
    cidr_blocks = "0.0.0.0/0"
    }
  ]

  tags = local.tags
}

resource "aws_eip" "nat" {
  count  = length(var.vpc_azs)
  domain = "vpc"
}

### EC2 instance for Web Server
module "ec2_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "5.1.2"

  name        = "${var.env}-web-sg"
  description = "Security group for web server"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = module.vpc.private_subnets_cidr_blocks
}

module "ec2_instance" {
  source = "terraform-aws-modules/ec2-instance/aws"

  name = "web-${var.env}"

  instance_type          = "t2.micro"
  key_name               = "user1"
  monitoring             = true
  vpc_security_group_ids = [module.ec2_sg.security_group_id]
  subnet_id              = var.vpc_private_subnets[0]

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

### Route53
module "zones" {
  source  = "terraform-aws-modules/route53/aws//modules/zones"
  version = "~> 2.0"

  zones = {
    "${var.hosted_zone}" = {
      tags = local.tags
    }
  }

  tags = local.tags
}

module "records" {
  source  = "terraform-aws-modules/route53/aws//modules/records"
  version = "~> 2.0"

  zone_name = module.zones.route53_zone_name[var.hosted_zone]

  records = [
    {
      name    = ""
      type    = "CNAME"
      ttl     = "300"
      records = [module.alb.lb_dns_name]
    },
  ]

  depends_on = [module.zones]
}

### Certificate
module "acm" {
  source  = "terraform-aws-modules/acm/aws"
  version = "~> 4.0"

  zone_id             = module.zones.route53_zone_zone_id[var.hosted_zone]
  domain_name         = var.hosted_zone
  wait_for_validation = false
  validation_method   = "DNS"

  tags = local.tags
}

### Application Load Balancer

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 8.7.0"

  name    = "${var.env}-web-alb"
  vpc_id  = module.vpc.vpc_id
  subnets = var.vpc_public_subnets

  security_group_rules = {
    ingress_all_http = {
      type        = "ingress"
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      description = "HTTP web traffic"
      cidr_blocks = ["0.0.0.0/0"]
    }

    egress_all = {
      type        = "egress"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  access_logs = {
    bucket = var.alb_access_log_bucket
  }

  target_groups = [
    {
      name_prefix      = "web-"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = "instance"
      health_check = {
        path                = "/"
        healthy_threshold   = 2
        unhealthy_threshold = 10
        timeout             = 5
        interval            = 10,
        matcher             = "200,301,302,404"
      }
    }
  ]

  http_tcp_listeners = [
    {
      port        = 80
      protocol    = "HTTP"
      action_type = "redirect"
      redirect = {
        port        = "443"
        protocol    = "HTTPS"
        status_code = "HTTP_301"
      }
    }
  ]

  https_listeners = [
    {
      port               = 443
      protocol           = "HTTPS"
      certificate_arn    = module.acm.acm_certificate_arn
      target_group_index = 0
    }
  ]

  tags = local.tags
}
